#include "znn/types.hpp"

using namespace znn;
void simple_gemm(float const* __restrict A, float const* __restrict B,
                 float* __restrict C, long_t M, long_t N, long_t K, long_t lda,
                 long_t ldb, long_t ldc, long_t alpha = 1, long_t beta = 1)
{

    for (int m = 0; m < M; m += 1)
    {
        for (int n = 0; n < N; n += 1)
        {
            if (beta == 0)
            {
                C[m * ldc + n] = 0;
            }

            for (int k = 0; k < K; k += 1)
            {
                if (alpha == 1)
                {
                    C[m * ldc + n] += A[m * lda + k] * B[k * ldb + n];
                }
                else
                {
                    C[m * ldc + n] -= A[m * lda + k] * B[k * ldb + n];
                }
            }
        }
    }
}
