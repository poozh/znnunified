#pragma once

#include <cstdlib>
#include <iostream>

#define ZNN_JIT_STRINGIFY_0(s) #s
#define ZNN_JIT_STRINGIFY(s) ZNN_JIT_STRINGIFY_0(s)

#define ZNN_JIT_ASSERT(condition)                                              \
    if (!(condition))                                                          \
    {                                                                          \
        std::cout << "Assertion " << ZNN_JIT_STRINGIFY(condition)              \
                  << " failed "                                                \
                  << "file: " << __FILE__ << " line: " << __LINE__             \
                  << std::endl;                                                \
        std::abort();                                                          \
    }                                                                          \
    static_cast<void>(0)

#if defined(NDEBUG) || defined(ZNN_NO_DEBUG)

#define ZNN_JIT_DEBUG_ASSERT(condition) static_cast<void>(0)

#else

#define ZNN_JIT_DEBUG_ASSERT(condition) ZNN_JIT_ASSERT(condition)

#endif
