#pragma once

#include "znn/fft/codelets/codelets.hpp"

namespace znn::fft
{

template <long_t KD, long_t KH, long_t KW, long_t TD, long_t TH, long_t TW,
          long_t DS, long_t HS, long_t WS, long_t OS>
struct kernel_fft
{
    static void forward(float const* __restrict in, float* __restrict out,
                        float* __restrict buf)
    {
        static constexpr long_t D_TS = TD;
        static constexpr long_t H_TS = TH;
        static constexpr long_t W_TS = TW / 2 + 1;

        SIMD_FLOAT* __restrict buffer = reinterpret_cast<SIMD_FLOAT*>(buf);

// First along width
#pragma unroll(KD)
        for (long_t d = 0; d < KD; ++d)
        {
#pragma unroll(KH)
            for (long_t h = 0; h < KH; ++h)
            {
                r2cf<TW, KW, WS / SIMD_WIDTH, 2>(
                    reinterpret_cast<SIMD_FLOAT const*>(in + d * DS + h * HS),
                    buffer + (d * H_TS * W_TS + h * W_TS) * 2);
            }
        }

        if
            constexpr(KH > 1)
            {
                for (long_t d = 0; d < KD; ++d)
                {
#pragma unroll(W_TS)
                    for (long_t w = 0; w < W_TS; ++w)
                    {
                        c2cf<H_TS, KH, W_TS * 2>(buffer +
                                                 (d * H_TS * W_TS + w) * 2);
                    }
                }
            }

        if
            constexpr(KD > 1)
            {
                for (long_t h = 0; h < H_TS; ++h)
                {
#pragma unroll(W_TS)
                    for (long_t w = 0; w < W_TS; ++w)
                    {
                        c2cf<TD, KD, H_TS * W_TS * 2>(buffer +
                                                      (h * W_TS + w) * 2);
                    }
                }
            }

        // Stream the output
        static constexpr long_t TOTAL_ELEMENTS = D_TS * H_TS * W_TS;

#pragma unroll(TOTAL_ELEMENTS)
        for (long_t e = 0; e < TOTAL_ELEMENTS; ++e)
        {
            SIMD_STREAM(out + e * OS, buffer[e * 2]);
            SIMD_STREAM(out + e * OS + SIMD_WIDTH, buffer[e * 2 + 1]);
        }
    }
};
}
