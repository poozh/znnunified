#pragma once

#include <sstream>
#include <string>

#define ZNN_STRINGIFY_0(s) #s
#define ZNN_STRINGIFY(s) ZNN_STRINGIFY_0(s)

#define DIE(message)                                                           \
    {                                                                          \
        std::cout << message << std::endl                                      \
                  << "file: " << __FILE__ << " line: " << __LINE__             \
                  << std::endl;                                                \
        abort();                                                               \
    }                                                                          \
    static_cast<void>(0)

#define UNIMPLEMENTED()                                                        \
    {                                                                          \
        std::cout << "unimplemented function" << std::endl                     \
                  << "file: " << __FILE__ << " line: " << __LINE__             \
                  << std::endl;                                                \
        abort();                                                               \
    }                                                                          \
    static_cast<void>(0)

#define STRONG_ASSERT(condition)                                               \
    if (!(condition))                                                          \
    {                                                                          \
        std::cout << "Assertion " << ZNN_STRINGIFY(condition) << " failed "    \
                  << "file: " << __FILE__ << " line: " << __LINE__             \
                  << std::endl;                                                \
        abort();                                                               \
    }                                                                          \
    static_cast<void>(0)

#if defined(NDEBUG) || defined(ZNN_NO_DEBUG)
#define WEAK_ASSERT(condition) static_cast<void>(0)
#else
#define WEAK_ASSERT(condition) STRONG_ASSERT(condition)
#endif

#define ZNN_ASSERT(condition) WEAK_ASSERT(condition)

template <typename F, typename L>
inline std::string ___this_file_this_line(const F& f, const L& l)
{
    std::ostringstream oss;
    oss << "\nfile: " << f << "\nline: " << l << "\n";
    return oss.str();
}

#define HERE() ___this_file_this_line(__FILE__, __LINE__)
