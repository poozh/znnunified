#pragma once

namespace znn
{
namespace detail
{
namespace tensor
{

struct random_initialize_tag
{
};

struct normal_initialize_tag
{
};

struct host_tag
{
};
struct device_tag
{
};
struct hbw_tag
{
};

struct one_init_tag
{
};
struct zero_init_tag
{
};

template <typename T>
struct type_t
{
};
}
}
} // namespace znn::detail::tensor
