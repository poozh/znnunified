#pragma once

#include <cstddef>
#include <mutex>
#include <random>

namespace znn::detail::tensor
{

template <typename T>
void random_initialize(T* ptr, std::size_t n,
                       T d = static_cast<T>(0.1)) noexcept
{
    static std::mt19937 rng = std::mt19937(1234);
    static std::mutex   m;

    std::uniform_real_distribution<T> dis(-d, d);

    {
        guard g(m);

        for (std::size_t i = 0; i < n; ++i)
        {
            ptr[i] = dis(rng);
        }
    }
}

template <typename T>
void random_initialize_normal(T* ptr, std::size_t n, T mean, T stdev) noexcept
{
    static std::mt19937 rng = std::mt19937(1234);
    static std::mutex   m;

    std::normal_distribution<T> dis(mean, stdev);

    {
        guard g(m);

        for (std::size_t i = 0; i < n; ++i)
        {
            ptr[i] = dis(rng);
        }
    }
}

} // namespace znn::detail::tensor
