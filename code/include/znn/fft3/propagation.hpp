#pragma once

#include "znn/fft3/input_transform/transform.hpp"
#include "znn/fft3/output_transform/transform.hpp"
#include "znn/fft3/pointwise/pointwise.hpp"
#include "znn/util/kernel_launcher.hpp"

namespace znn::fft3
{

template <long_t Cores, long_t Threads, class Layer,
          bool TransformKernels = true, bool HTTransforms = false>
class propagation
{
private:
    static inline constexpr long_t cacheline_pad(long_t s)
    {
        return ((s + CACHELINE_SIZE - 1) / CACHELINE_SIZE) * CACHELINE_SIZE;
    }

    static constexpr long_t o_offset =
        cacheline_pad(Layer::matrices::Bs::memory_in_floats);

    static constexpr long_t c_offset =
        o_offset + cacheline_pad(Layer::matrices::As::memory_in_floats);

    static constexpr long_t c_f_offset =
        c_offset + cacheline_pad(Layer::matrices::Cs::memory_in_floats);

public:
    static constexpr long_t buffer_floats =
        cacheline_pad(c_f_offset + Layer::output_transform::buffer_memory);

    static constexpr long_t buffer_memory = buffer_floats * sizeof(float);

private:
    kernel_launcher launcher;

    input_transform::transform<Cores, Layer, TransformKernels, HTTransforms>
        input_trans;
    pointwise::mmm<Cores, (Threads == 2), Layer>            pointwise_comp;
    output_transform::transform<Cores, Layer, HTTransforms> output_trans;

public:
    propagation(bool apf1 = true, bool bpf1 = true)
        : launcher(Cores, 2)
        , input_trans(launcher)
        , pointwise_comp(launcher, apf1, bpf1)
        , output_trans(launcher)
    {
    }

    vec<double, 3> complexity() const
    {
        return {input_trans.gbytes(), pointwise_comp.gflops(),
                output_trans.gbytes()};
    }

    vec<double, 3> actual_complexity() const
    {
        return {input_trans.gbytes(), pointwise_comp.actual_gflops(),
                output_trans.gbytes()};
    }

    vec<double, 3> execute(float const* __restrict in,
                           float const* __restrict ker, float* __restrict out,
                           float* __restrict buffer)
    {
        auto begin = std::chrono::high_resolution_clock::now();

        if
            constexpr(TransformKernels)
            {
                // input transform
                input_trans.execute(in, buffer + o_offset, ker, buffer);
            }
        else
        {
            // input transform
            input_trans.execute(in, buffer + o_offset);
            static_cast<void>(ker);
        }

        auto end = std::chrono::high_resolution_clock::now();

        auto time1 = duration_in_ms(end - begin);

        // pointwise
        pointwise_comp(buffer + o_offset, buffer, buffer + c_offset,
                       buffer + c_f_offset);

        begin = std::exchange(end, std::chrono::high_resolution_clock::now());
        auto time2 = duration_in_ms(end - begin);

        // output
        output_trans.execute(buffer + c_f_offset, out);

        begin = std::exchange(end, std::chrono::high_resolution_clock::now());
        auto time3 = duration_in_ms(end - begin);

        return {time1, time2, time3};
    }
};

} // namespace znn::fft3
