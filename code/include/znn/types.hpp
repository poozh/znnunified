#pragma once

#include <chrono>
#include <cstddef>
#include <cstdint>
#include <mutex>

#include "compiler.hpp"
#include "znn/vec.hpp"

#if !defined(ZNN_CACHE_SIZE)
#define ZNN_CACHE_SIZE 512
#endif

namespace znn
{

typedef std::int64_t long_t;
typedef std::size_t  size_t;

using zi2::vl::vec;

typedef vec<long_t, 2> vec2i;
typedef vec<long_t, 3> vec3i;
typedef vec<long_t, 4> vec4i;
typedef vec<long_t, 5> vec5i;

using zi2::vl::subvec;

template <long_t... Ints>
using vek = zi2::vl::vec_type<long_t, Ints...>;

typedef std::lock_guard<std::mutex> guard;

template <typename T>
inline double duration_in_ms(T const& interval)
{
    return static_cast<double>(
               std::chrono::duration_cast<std::chrono::microseconds>(interval)
                   .count()) /
           1000;
}

} // namespace znn
