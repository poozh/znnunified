#pragma once

#include <sstream>
#include <string>

// GCC version
#if defined(__GNUC__) && !defined(GCC_VERSION) && !defined(__clang__)
#define GCC_VERSION                                                            \
    ((__GNUC__)*10000 + (__GNUC_MINOR__)*100 + (__GNUC_PATCHLEVEL__))
#endif

// Clang version
#if defined(__clang__)
#define CLANG_VERSION                                                          \
    ((__clang_major__)*10000 + (__clang_minor__)*100 + (__clang_patchlevel__))
#endif

// ICC version
#if defined(__INTEL_COMPILER)
#define ICC_VERSION __INTEL_COMPILER
#endif

inline std::string ___znn__compiler_verson(int v)
{
    return std::to_string(v / 10000) + "." + std::to_string((v % 10000) / 100) +
           "." + std::to_string(v % 100);
}

namespace znn
{

inline std::string compiler_version()
{
    std::string ret;

#if defined(__INTEL_COMPILER)
    ret += "icc " + std::to_string(__INTEL_COMPILER) + " compat with ";
#endif

#if defined(__GNUC__) && !defined(__clang__)
    ret += "gcc ";
    ret += ___znn__compiler_verson(GCC_VERSION);
#elif defined(__clang__)
    ret += "clang ";
    ret += ___znn__compiler_verson(CLANG_VERSION);
#endif
    return ret;
}

} // namespace znn
