#include "znn/fft3/bench.hpp"

using namespace znn::fft3;
using namespace znn;

int main()
{
    using F  = vek<1, 8, 8>;
    using K3 = vek<1, 3, 3>;
    using K5 = vek<1, 5, 5>;

    do_bench<ZNN_USE_CORES, 128, 96, 256, 1, 28, 28, F, K5>("OverFeat 2");
    do_bench<ZNN_USE_CORES, 128, 256, 512, 1, 14, 14, F, K3>("OverFeat 3");
    do_bench<ZNN_USE_CORES, 128, 512, 1024, 1, 14, 14, F, K3>("OverFeat 4");
    do_bench<ZNN_USE_CORES, 128, 1024, 1024, 1, 14, 14, F, K3>("OverFeat 5");
}
