#include "znn/fft3/bench.hpp"

using namespace znn::fft3;
using namespace znn;

int main()
{
    using F  = vek<1, 11, 11>;
    using K5 = vek<1, 5, 5>;

    do_bench<ZNN_USE_CORES, 128, 64, 192, 1, 31, 31, F, K5>("AlexNet 2");
}
