#include "znn/fft3/bench.hpp"

using namespace znn::fft3;
using namespace znn;

int main()
{
    using FS = vek<1, 27, 27>;
    using KS = vek<1, 3, 3>;

    do_bench<ZNN_USE_CORES, 64, 64, 64, 1, 226, 226, FS, KS>("VGG 1.2");
}
