#include "znn/fft3/bench.hpp"

using namespace znn::fft3;
using namespace znn;

int main()
{
    using F  = vek<1, 9, 9>;
    using K3 = vek<1, 3, 3>;
    using K5 = vek<1, 5, 5>;

    do_bench<ZNN_USE_CORES, 128, 64, 192, 1, 31, 31, F, K5>("AlexNet 2");
    do_bench<ZNN_USE_CORES, 128, 192, 384, 1, 15, 15, F, K3>("AlexNet 3");
    do_bench<ZNN_USE_CORES, 128, 384, 256, 1, 15, 15, F, K3>("AlexNet 4");
    do_bench<ZNN_USE_CORES, 128, 256, 256, 1, 15, 15, F, K3>("AlexNet 5");
}
