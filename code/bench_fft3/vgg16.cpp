#include "znn/fft3/bench.hpp"

using namespace znn::fft3;
using namespace znn;

int main()
{
    using FS = vek<1, 16, 16>;
    using KS = vek<1, 3, 3>;

    do_bench<ZNN_USE_CORES, 64, 64, 64, 1, 226, 226, FS, KS>("VGG 1.2");
    do_bench<ZNN_USE_CORES, 64, 64, 128, 1, 114, 114, FS, KS>("VGG 2.1");
    do_bench<ZNN_USE_CORES, 64, 128, 128, 1, 114, 114, FS, KS>("VGG 2.2");
    do_bench<ZNN_USE_CORES, 64, 128, 256, 1, 58, 58, FS, KS>("VGG 3.1");
    do_bench<ZNN_USE_CORES, 64, 256, 256, 1, 58, 58, FS, KS>("VGG 3.2");
    do_bench<ZNN_USE_CORES, 64, 256, 512, 1, 30, 30, FS, KS>("VGG 4.1");
    do_bench<ZNN_USE_CORES, 64, 512, 512, 1, 30, 30, FS, KS>("VGG 4.2");
    do_bench<ZNN_USE_CORES, 64, 512, 512, 1, 16, 16, FS, KS>("VGG 5");
}
