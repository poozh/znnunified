#include "znn/win/bench.hpp"

using namespace znn;
using namespace znn::win;

int main()
{
    using M = vek<1, 5, 5>;
    using K3 = vek<1, 3, 3>;

    do_bench<ZNN_USE_CORES, 128, 192, 384, 1, 15, 15, M, K3>("AlexNet 3");
    do_bench<ZNN_USE_CORES, 128, 384, 256, 1, 15, 15, M, K3>("AlexNet 4");
    do_bench<ZNN_USE_CORES, 128, 256, 256, 1, 15, 15, M, K3>("AlexNet 5");
}
