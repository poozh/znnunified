#include "znn/win/bench.hpp"

using namespace znn;
using namespace znn::win;

int main()
{
    using M = vek<1, 6, 6>;
    using K = vek<1, 3, 3>;

    do_bench<ZNN_USE_CORES, 64, 64, 64, 1, 226, 226, M, K>("VGG 1.2");
    do_bench<ZNN_USE_CORES, 64, 64, 128, 1, 114, 114, M, K>("VGG 2.1");
    do_bench<ZNN_USE_CORES, 64, 128, 128, 1, 114, 114, M, K>("VGG 2.2");
    do_bench<ZNN_USE_CORES, 64, 128, 256, 1, 58, 58, M, K>("VGG 3.1");
    do_bench<ZNN_USE_CORES, 64, 256, 256, 1, 58, 58, M, K>("VGG 3.2");
    do_bench<ZNN_USE_CORES, 64, 256, 512, 1, 30, 30, M, K>("VGG 4.1");
    do_bench<ZNN_USE_CORES, 64, 512, 512, 1, 30, 30, M, K>("VGG 4.2");
    do_bench<ZNN_USE_CORES, 64, 512, 512, 1, 16, 16, M, K>("VGG 5");
}
