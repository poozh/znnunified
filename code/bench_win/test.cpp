#include "znn/win/bench.hpp"

using namespace znn;
using namespace znn::win;

int main()
{

    using M6 = vek<1, 6, 6>;
    using M5 = vek<1, 5, 5>;
    using K3 = vek<1, 3, 3>;
    using K5 = vek<1, 5, 5>;

    do_bench<ZNN_USE_CORES, 256, 64, 256, 1, 32, 32, M5, K5, false>(
        "AlexNet 2");
    do_bench<ZNN_USE_CORES, 256, 64, 256, 1, 32, 32, M6, K5, false>(
        "AlexNet 2");
    do_bench<ZNN_USE_CORES, 256, 256, 384, 1, 16, 16, M5, K3, false>(
        "AlexNet 3");
    do_bench<ZNN_USE_CORES, 256, 384, 256, 1, 16, 16, M5, K3, false>(
        "AlexNet 4");
    do_bench<ZNN_USE_CORES, 256, 256, 256, 1, 16, 16, M5, K3, false>(
        "AlexNet 5");
}
