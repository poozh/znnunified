#include "znn/win/bench.hpp"

using namespace znn;
using namespace znn::win;

int main()
{
    using M  = vek<1, 5, 5>;
    using K3 = vek<1, 3, 3>;

    do_bench<ZNN_USE_CORES, 128, 256, 512, 1, 14, 14, M, K3>("OverFeat 3");
    do_bench<ZNN_USE_CORES, 128, 512, 1024, 1, 14, 14, M, K3>("OverFeat 4");
    do_bench<ZNN_USE_CORES, 128, 1024, 1024, 1, 14, 14, M, K3>("OverFeat 5");
}
