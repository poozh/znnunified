#include "znn/win/bench.hpp"

using namespace znn;
using namespace znn::win;

int main()
{
    using M = vek<1, 5, 5>;
    using K = vek<1, 3, 3>;

    do_bench<ZNN_USE_CORES, 1, 64, 64, 1, 640, 640, M, K>("FusionNet 1.2");
    do_bench<ZNN_USE_CORES, 1, 128, 128, 1, 320, 320, M, K>("FusionNet 1.3");
    do_bench<ZNN_USE_CORES, 1, 256, 256, 1, 160, 160, M, K>("FusionNet 1.4");
    do_bench<ZNN_USE_CORES, 1, 512, 512, 1, 80, 80, M, K>("FusionNet 1.5");
    do_bench<ZNN_USE_CORES, 1, 1024, 1024, 1, 40, 40, M, K>("FusionNet 1.5");
}
