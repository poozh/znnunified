#include "znn/fft/bench.hpp"

using namespace znn::fft;
using namespace znn;

int main()
{
    using F  = vek<1, 15, 15>;
    using K3 = vek<1, 3, 3>;

    do_bench<ZNN_USE_CORES, 128, 192, 384, 1, 15, 15, F, K3>("AlexNet 3");
    do_bench<ZNN_USE_CORES, 128, 384, 256, 1, 15, 15, F, K3>("AlexNet 4");
    do_bench<ZNN_USE_CORES, 128, 256, 256, 1, 15, 15, F, K3>("AlexNet 5");
}
