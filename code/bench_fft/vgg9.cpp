#include "znn/fft/bench.hpp"

using namespace znn::fft;
using namespace znn;

int main()
{
    using FS = vek<1, 9, 9>;
    using KS = vek<1, 3, 3>;

    do_bench<ZNN_USE_CORES, 64, 256, 512, 1, 30, 30, FS, KS>("VGG 4.1");
    do_bench<ZNN_USE_CORES, 64, 512, 512, 1, 30, 30, FS, KS>("VGG 4.2");
    do_bench<ZNN_USE_CORES, 64, 512, 512, 1, 16, 16, FS, KS>("VGG 5");
}
