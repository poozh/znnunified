#include "znn/fft/bench.hpp"

using namespace znn::fft;
using namespace znn;

int main()
{
    using FS = vek<1, 25, 25>;
    using KS = vek<1, 3, 3>;

    do_bench<ZNN_USE_CORES, 64, 64, 64, 1, 226, 226, FS, KS>("VGG 1.2");
    do_bench<ZNN_USE_CORES, 64, 64, 128, 1, 114, 114, FS, KS>("VGG 2.1");
    do_bench<ZNN_USE_CORES, 64, 128, 128, 1, 114, 114, FS, KS>("VGG 2.2");
}
