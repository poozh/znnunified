#include "znn/fft/bench.hpp"

using namespace znn::fft;
using namespace znn;

int main()
{
    using F  = vek<1, 10, 10>;
    using K5 = vek<1, 5, 5>;

    do_bench<ZNN_USE_CORES, 128, 96, 256, 1, 28, 28, F, K5>("OverFeat 2");
}
