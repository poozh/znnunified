#include "znn/fft/bench.hpp"

using namespace znn::fft;
using namespace znn;

int main()
{
    using F  = vek<1, 18, 18>;
    using K5 = vek<1, 5, 5>;

    do_bench<ZNN_USE_CORES, 128, 64, 192, 1, 31, 31, F, K5>("AlexNet 2");
}
